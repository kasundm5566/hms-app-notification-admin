# Appstore Notification Admin API Server

## Software Used

* Java 1.8
* Gradle 4.0.1
* Kotlin 1.1.3-2
* MySQL 5.7
* Postman 6.0.10

## DNS Configurations

Add following DNS entries to your `/etc/hosts` file.

```
127.0.0.1 db.mysql.appnotification
127.0.0.1 core.notification.server
```


## Setup initial data

* Create a MySQL user according to the following details.
    - username: user
    - password: password
* Create a new database 'appnotification_db' (only if there is no database named 'appnotification_db').
* Run the MySQL script **appnotifiaction_db.sql** in the `<project-location>/db-setup`.
* Please keep a backup of databases before applying dumps.

## Generate and retrieve Firebase access token

* Every Firebase project has a default service account. You can use this account to call Firebase server APIs from your 
app server or trusted environment. If you use a different service account, make sure it has Editor or Owner permissions.
 
* To authenticate the service account and authorize it to access Firebase services, 
you must generate a private key file in JSON format and use this key to retrieve a short-lived OAuth 2.0 token. 
Once you have a valid token, you can add it in your server requests as required by the various Firebase services such as Remote Config or FCM.

* Please refer the following link to find complete instructions about access token,
https://firebase.google.com/docs/cloud-messaging/migrate-v1

## Set date time

* Date and the time of the server which is going to run the notification server should have correct date and time according to the time zone.
If there is an issue with the date time in the server, it will throw an exception as mentioned below.

`Invalid JWT: Token must be a short-lived token (60 minutes) and in a reasonable timeframe. 
Check your iat and exp values and use a clock with skew to account for clock differences between systems.`

## How to build and create Distribution

* Run `./gradlew clean` to clean the project.

* Run `./gradlew -p appstore-notification-admin-api-service createTanukiWrapper`

* Distribution file will be created at `<project-location>/appstore-notification-admin-api-service/build/appstore-notification-admin-api-service`

## How to start the server

* Build as mentioned in the previous step.

* Go to `<project-location>/appstore-notification-admin-api-service/build/appstore-notification-admin-api-service/bin`

* Execute `./appstore-notification-admin-api-service start` to start the server

## Configurations

Configurations can be found in the application.yml file.

#### Configure MySQL
* `spring:datasource:url`: MySQL connection URL 
* `spring:datasource:username`: Username of the MySQL user
* `spring:datasource:password`: Password of the MySQL user

#### Server related configurations
* `notification-admin:app-store-all-users-fcm-topic`: Topic for the all users
* `notification-admin:notification-server:server-url:`: URL to connect App Notification Server
* `notification-admin:appstore-server-url-template`: App URL part of the app update message content
* `notification-admin:new-version-available-msg-content-template`: App Update message content
* `notification-admin:new-version-available-msg-title-template`: Title of the app update message
* `notification-admin:default-click-action-activity`: Click action of the Android app when a push notification is received
* `notification-admin:app-page-click-action-activity`: Click action of the Android app when a app update notification is received