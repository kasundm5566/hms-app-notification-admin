## Push notification support for Vodafone

## Provisioning Module

* API to notify external service when build file is approved by an admin.
  * External service url should be configurable
  * Parameters - App Id, Build file details
  * Print a log and snmp trap if notification sending fails

## AppStore Admin

### UI
* UI to push notifications
  * Message category -> ALL, APP Specific
  * Show "APP Specific" drop-down with list of available AppIds.
    * Need to figure out how to get this app list.
  * Admin can select more than one AppId.
  * Should be able to include URLs in the notification message.
  * Implement audit log for all the push notifications sent by admin
    * admin_user_name, date_time, category, app_id_list, message_content

### Discovery Server
* API for device registraion calls from app and pass those to Push notification server
* API to received build file aproval notifications from provisioning.
* Ability to broadcast notification message based on notification requests created by admins.

## Dataloader 

* Create new table to store app download details
  * Use data from `cms-translog`
  * msisdn, app_id, app_version, downloaded_date
  * One unique entry for (msisdn, app_id)
  * When new version is downloaded this table should be updated, no duplicate entries


## Push notification server

* Create FCM topic to include all AppStore users
* Create FCM topic based on AppId
* Delete FCM topic
* Add msisdn to a selected topic
* Remove msisdn from selected topic
* Push message to FCM topic
* Push message to individual FCM token
* Links sent in the message content should be clickable. 

