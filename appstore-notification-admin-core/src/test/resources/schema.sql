CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;

DROP TABLE IF EXISTS "user_profile" CASCADE ;
CREATE TABLE user_profile (
    user_id character varying(255) PRIMARY KEY,
    first_name character varying(255),
    last_name character varying(255),
    dob date,
    email character varying(255),
    auth_medium character varying(25) NOT NULL,
    external_user_id character varying(255) NOT NULL,
    updated_at timestamp without time zone,
    created_at timestamp without time zone
);

ALTER TABLE user_profile OWNER TO postgres;


DROP TABLE IF EXISTS "user_dashboard_component";
CREATE TABLE user_dashboard_component (
    id BIGSERIAL PRIMARY KEY NOT NULL,
    component_type character varying(255),
    placement character varying(255),
    user_id character varying(255) NOT NULL
);


ALTER TABLE user_dashboard_component OWNER TO postgres;

ALTER TABLE ONLY user_dashboard_component
    ADD CONSTRAINT fk_user_dashboard_user_id_to_user_profile_user_id FOREIGN KEY (user_id) REFERENCES user_profile(user_id);



DROP TABLE IF EXISTS "third_party_devices" CASCADE;
CREATE TABLE "third_party_devices" (
  user_id             CHARACTER VARYING(255),
  device_manufacturer CHARACTER VARYING(255)      NOT NULL,
  external_user_id    CHARACTER VARYING(255)      NOT NULL,
  access_token        CHARACTER VARYING(500)      NOT NULL,
  refresh_token       CHARACTER VARYING(500),
  token_expire        INTEGER                     NOT NULL,
  token_updated       TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  created_at          TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  updated_at          TIMESTAMP WITHOUT TIME ZONE,
  PRIMARY KEY (user_id, device_manufacturer)
);

ALTER TABLE third_party_devices
  OWNER TO postgres;