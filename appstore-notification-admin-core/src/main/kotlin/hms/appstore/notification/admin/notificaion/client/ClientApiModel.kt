package hms.appstore.notification.admin.notificaion.client

data class SendNotificationRequest(
        val requestId: String,
        val requestingSystemId: String,
        val userIds: Array<String>,
        val messageContent: NotificationContent,
        val dispatchType: String?,
        val clickAction: String,
        val additionalParameters: Map<String, String>
)

data class NotificationContent(
        val title: String,
        val body: String,
        val targetScreen: String
)

data class SendNotificationToAllRequest(
        val requestId: String,
        val requestingSystemId: String,
        val messageContent: NotificationContent
)

data class SendNotificationResponse(
        val requestId: String,
        val status: String
) {
    fun isSuccessful(): Boolean {
        return "S1000".equals(status)
    }
}

data class NotifyTopicRequest(
        val requestId: String,
        val requestingSystemId: String,
        val topic: String,
        val messageContent: NotificationContent,
        val dispatchType: String?,
        val clickAction: String
)

data class NotifyTopicResponse(
        val requestId: String,
        val messageId: String,
        val error: Int?,
        val topic: String
)