package hms.appstore.notification.admin.impl

import hms.appstore.notification.admin.AppNotificationService
import hms.appstore.notification.admin.broadcast.BroadcastTaskCreator
import hms.appstore.notification.admin.model.AppSpecific
import hms.appstore.notification.admin.model.NotifyUserResponse
import hms.appstore.notification.admin.model.NotifyUsersRequest
import hms.appstore.notification.admin.notificaion.client.NotificationApiClient
import hms.commons.IdGenerator
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import reactor.core.scheduler.Schedulers

@Service
@ConfigurationProperties(prefix = "notification-admin")
class AppNotificationServiceImpl(val broadcastTaskCreator: BroadcastTaskCreator,
                                 val notificationApiClient: NotificationApiClient) : AppNotificationService {

    lateinit var appStoreAllUsersFcmTopic: String
    lateinit var defaultClickActionActivity: String

    companion object {
        val LOGGER: Logger = LoggerFactory.getLogger(AppNotificationServiceImpl::class.java)
    }


    override fun notifyUsers(notificationRequest: Mono<NotifyUsersRequest>): Mono<NotifyUserResponse> {

        return notificationRequest
                .doOnSuccess { request -> LOGGER.info("Request received to dispatch notifications [$request]") }
                .flatMap { request -> dispatchNotification(request) }

    }

    private fun dispatchNotification(request: NotifyUsersRequest): Mono<NotifyUserResponse> {
        val recipient = request.recipient
        if (recipient is AppSpecific) {
            LOGGER.debug("Notifications are app specific")
            val brTaskId = broadcastTaskCreator.createAppSpecificNotificationTask(request, recipient)
            return Mono.just(NotifyUserResponse(brTaskId))
        } else {
            LOGGER.debug("Notifications are for all appstore users using topic [$appStoreAllUsersFcmTopic]")
            return notificationApiClient.sendNotificationToAllUsers(IdGenerator.generateId(), appStoreAllUsersFcmTopic,
                    request.messageTitle, request.messageContent, request.dispatchType, defaultClickActionActivity)
                    .map { resp -> NotifyUserResponse(resp.requestId) }

        }
    }
}