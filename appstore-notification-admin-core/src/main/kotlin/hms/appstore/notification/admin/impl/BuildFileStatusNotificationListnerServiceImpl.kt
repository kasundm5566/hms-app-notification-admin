package hms.appstore.notification.admin.impl

import hms.appstore.notification.admin.BuildFileStatusNotificationListnerService
import hms.appstore.notification.admin.broadcast.BroadcastTaskCreator
import hms.appstore.notification.admin.model.BuildFileStatusRequest
import hms.appstore.notification.admin.model.BuildFileStatusResponse
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono

@Component
class BuildFileStatusNotificationListnerServiceImpl(val brTaskCreator: BroadcastTaskCreator) : BuildFileStatusNotificationListnerService {

    companion object {
        val LOGGER: Logger = LoggerFactory.getLogger(BuildFileStatusNotificationListnerServiceImpl::class.java)
    }

    override fun buildFileStatusUpdate(buildFileStatusRequest: Mono<BuildFileStatusRequest>): Mono<BuildFileStatusResponse> {
        return buildFileStatusRequest
                .doOnSuccess { request -> LOGGER.info("Build file status update received [$request]") }
                .filter { request -> "approved".equals(request.status) }
                .map { request -> brTaskCreator.createBrTaskForNewBuildFile(request) }
                .map { taskId -> BuildFileStatusResponse("message-created", taskId) }
                .switchIfEmpty(Mono.just(BuildFileStatusResponse("message-not-created", "-1")))
    }
}