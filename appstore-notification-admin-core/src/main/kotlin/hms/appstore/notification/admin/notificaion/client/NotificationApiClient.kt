package hms.appstore.notification.admin.notificaion.client

import reactor.core.publisher.Mono

interface NotificationApiClient {

    /**
     * Send notification to given list of users
     */
    fun sendIndividualNotifications(requestId: String, userIdList: Array<String>, messageTitile: String, messageContent: String, dispatchType: String?, clickAction: String, additionalParameters: Map<String, String>): Mono<SendNotificationResponse>

    /**
     * Send notification to all the users
     */
    fun sendNotificationToAllUsers(requestId: String, topic: String, title: String, messageContent: String, dispatchType: String?, clickAction: String): Mono<SendNotificationResponse>
}