/*
 * (C) Copyright 2010- 2017 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.notification.admin

import hms.appstore.notification.admin.model.NotifyUserResponse
import hms.appstore.notification.admin.model.NotifyUsersRequest
import reactor.core.publisher.Mono

interface AppNotificationService {
    /**
     * Send notification messages to users.
     */
    fun notifyUsers(notificationRequest: Mono<NotifyUsersRequest>): Mono<NotifyUserResponse>
}