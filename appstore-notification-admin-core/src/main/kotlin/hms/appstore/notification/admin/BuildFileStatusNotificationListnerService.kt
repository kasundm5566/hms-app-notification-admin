package hms.appstore.notification.admin

import hms.appstore.notification.admin.model.BuildFileStatusRequest
import hms.appstore.notification.admin.model.BuildFileStatusResponse
import reactor.core.publisher.Mono

interface BuildFileStatusNotificationListnerService {

    fun buildFileStatusUpdate(buildFileStatus: Mono<BuildFileStatusRequest>):Mono<BuildFileStatusResponse>
}