package hms.appstore.notification.admin.broadcast

import hms.appstore.notification.admin.model.AppSpecific
import hms.appstore.notification.admin.model.BuildFileStatusRequest
import hms.appstore.notification.admin.model.NotifyUsersRequest
import hms.common.broadcast.domain.BroadcastRequest
import hms.common.broadcast.domain.ExecutionState
import hms.common.broadcast.domain.Task
import hms.common.broadcast.repo.TaskRepository
import hms.commons.IdGenerator
import org.joda.time.DateTime
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.stereotype.Component
import java.text.MessageFormat

@Component
@ConfigurationProperties(prefix = "notification-admin")
@EnableConfigurationProperties
class BroadcastTaskCreator(val taskRepository: TaskRepository) {
    companion object {
        val LOGGER: Logger = LoggerFactory.getLogger(BroadcastTaskCreator::class.java)
    }

    lateinit var newVersionAvailableMsgContentTemplate: String
    lateinit var newVersionAvailableMsgTitleTemplate: String
    lateinit var appstoreServerUrlTemplate: String
    lateinit var defaultClickActionActivity: String
    lateinit var appPageClickActionActivity: String

    fun createBrTaskForNewBuildFile(buildFileStatusNotify: BuildFileStatusRequest): String {
        val taskId = IdGenerator.generateId()

        val task = Task(taskId)
        task.state = ExecutionState.PENDING
        task.createdBy = "new-build-file-event"
        task.isUseSubmitMultiDispatch = false

        val brRequest = BroadcastRequest()
        brRequest.receivedTime = DateTime.now()
        brRequest.correlationId = buildFileStatusNotify.requestId
        brRequest.subscriberFetchCriteria = hashMapOf<String, Any>("appId" to buildFileStatusNotify.appId)
        brRequest.dispatcherId = buildFileStatusNotify.appId
        val messageContent = createDownloadNewVersionMessage(buildFileStatusNotify.appId, buildFileStatusNotify.appName, buildFileStatusNotify.buildVersion)
        val messageTitle = createDownloadNewVersionMessageTitle(buildFileStatusNotify.appName, buildFileStatusNotify.buildVersion)


        if(buildFileStatusNotify.dispatchType != null){
            brRequest.messageData = hashMapOf<String, Any>("message" to messageContent,
                    "title" to messageTitle, "dispatchType" to buildFileStatusNotify.dispatchType.toString(), "clickActivity" to appPageClickActionActivity)
        }else{
            brRequest.messageData = hashMapOf<String, Any>("message" to messageContent,
                    "title" to messageTitle, "clickActivity" to appPageClickActionActivity)
        }

        task.broadcastRequest = brRequest

        taskRepository.saveTask(task)

        LOGGER.info("Broadcast task created [$task]")

        return taskId
    }

    private fun createDownloadNewVersionMessage(appId: String, appName: String, newVersionNumber: String): String {
        val url = MessageFormat.format(appstoreServerUrlTemplate, *arrayOf(appId))
        val messageContent  = MessageFormat.format(newVersionAvailableMsgContentTemplate, *arrayOf(appName, url))
        return messageContent
    }

    private fun createDownloadNewVersionMessageTitle(appName: String, newVersionNumber: String): String {
        return newVersionAvailableMsgTitleTemplate
    }

    fun createAppSpecificNotificationTask(request: NotifyUsersRequest, recipient: AppSpecific): String {
        LOGGER.info("Creating broadcast task for app specific notification request [$request]")

        val taskId = IdGenerator.generateId()

        val task = Task(taskId)
        task.state = ExecutionState.PENDING
        task.createdBy = request.createdBy
        task.isUseSubmitMultiDispatch = false

        val brRequest = BroadcastRequest()
        brRequest.receivedTime = DateTime.now()
        brRequest.correlationId = request.requestId

        brRequest.subscriberFetchCriteria = hashMapOf<String, Any>("appId" to recipient.appId)
        brRequest.dispatcherId = recipient.appId
        if(request.dispatchType != null){
            brRequest.messageData = hashMapOf<String, Any>("message" to request.messageContent, "title" to request.messageTitle, "dispatchType" to request.dispatchType.toString(), "clickActivity" to defaultClickActionActivity)
        }else{
            brRequest.messageData = hashMapOf<String, Any>("message" to request.messageContent, "title" to request.messageTitle, "clickActivity" to defaultClickActionActivity)
        }

        task.broadcastRequest = brRequest

        taskRepository.saveTask(task)

        LOGGER.info("Broadcast task created [$task]")

        return taskId
    }

}