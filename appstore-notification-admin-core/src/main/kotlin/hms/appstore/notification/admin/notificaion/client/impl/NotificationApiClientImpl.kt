package hms.appstore.notification.admin.notificaion.client.impl

import hms.appstore.notification.admin.notificaion.client.*
import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono
import javax.annotation.PostConstruct

@Component
@ConfigurationProperties(prefix = "notification-admin.notification-server")
@EnableConfigurationProperties
class NotificationApiClientImpl : NotificationApiClient {

    var serverUrl: String? = ""
    private var client: WebClient? = null

    companion object {
        val LOGGER = LoggerFactory.getLogger(NotificationApiClientImpl::class.java)
    }

    @PostConstruct
    fun initClient() {
        val url = serverUrl ?: ""
        LOGGER.debug("Creating web client for [$url]")
        client = WebClient.builder()
                .baseUrl(url)
                .build()
    }

    override fun sendIndividualNotifications(requestId: String, userIdList: Array<String>, messageTitile: String, messageContent: String, dispatchType: String?, clickAction: String, additionalParameters: Map<String, String>): Mono<SendNotificationResponse> {
        LOGGER.info("Notification Request with [$messageContent] to [${userIdList.contentToString()}]")
        val webClient = client
        if (webClient != null) {
            return webClient.post()
                    .uri("/notify")
                    .header(HttpHeaders.CONTENT_TYPE,
                            MediaType.APPLICATION_JSON_VALUE)
                    .body(BodyInserters.fromObject(getIndividualNotificationApiRequest(requestId, userIdList, messageTitile, messageContent, dispatchType, clickAction, additionalParameters)))
                    .retrieve()
                    .bodyToMono(SendNotificationResponse::class.java)
                    .doOnError { error -> LOGGER.error("Error Response received [ANDROID] [$error]") }
                    .doOnSuccess { success -> LOGGER.info("Success response received [ANDROID] [$success]") }
        } else {
            return Mono.just(SendNotificationResponse(requestId, "E1000"))
        }
    }

    override fun sendNotificationToAllUsers(requestId: String, topic: String, title: String, messageContent: String, dispatchType: String?, clickAction: String): Mono<SendNotificationResponse> {
        LOGGER.info("Notification Request with [$messageContent] to all users")
        val webClient = client
        if (webClient != null) {
            return webClient.post()
                    .uri("/topic")
                    .header(HttpHeaders.CONTENT_TYPE,
                            MediaType.APPLICATION_JSON_VALUE)
                    .body(BodyInserters.fromObject(getAllUsersNotificationApiRequest(requestId, topic, title, messageContent, dispatchType, clickAction)))
                    .retrieve()
                    .bodyToMono(SendNotificationResponse::class.java)
                    .doOnError { error -> LOGGER.error("Error Response received [ANDROID] [$error]") }
                    .doOnSuccess { success -> LOGGER.info("Success response received [ANDROID] [$success]") }
        } else {
            return Mono.just(SendNotificationResponse(requestId, "E1000"))
        }
    }

    private fun getIndividualNotificationApiRequest(requestId: String, userIdList: Array<String>, messageTitile: String, messageContent: String, dispatchType: String?, clickAction: String, additionalParameters: Map<String, String>): SendNotificationRequest {
        return SendNotificationRequest(requestId,
                "notification-admin",
                userIdList,
                NotificationContent(messageTitile, messageContent, ""), dispatchType, clickAction, additionalParameters)
    }

    private fun getAllUsersNotificationApiRequest(requestId: String, topic: String, title: String,
                                                  messageContent: String, dispatchType: String?, clickAction: String): NotifyTopicRequest {
        return NotifyTopicRequest(requestId,
                "notification-admin",
                topic,
                NotificationContent(title, messageContent, ""), dispatchType, clickAction)
    }
}