package hms.appstore.notification.admin.broadcast

import hms.appstore.notification.admin.notificaion.client.NotificationApiClient
import hms.common.broadcast.domain.DispatchResult
import hms.common.broadcast.domain.Subscriber
import hms.common.broadcast.domain.Task
import hms.common.broadcast.engine.MessageSender
import hms.common.broadcast.repo.SubscriberRepository
import hms.common.broadcast.repo.TaskRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Component
class NotificationSender(val taskRepo: TaskRepository,
                         val subscriberRepo: SubscriberRepository,
                         val notificationApiClient: NotificationApiClient) : MessageSender(taskRepo, subscriberRepo, 0) {

    companion object {
        val LOGGER: Logger = LoggerFactory.getLogger(NotificationSender::class.java)
    }

    override fun doSend(subscriber: Subscriber, task: Task): DispatchResult {
        LOGGER.info("Sending notification to [${subscriber.id}] for task [${task.taskId}]")
        val messageData = task.broadcastRequest.messageData["message"] as String
        val messageTitle = task?.broadcastRequest?.messageData?.get("title") as String
        val dispatchType = task?.broadcastRequest?.messageData?.get("dispatchType") as String?
        val clickAction = task?.broadcastRequest?.messageData?.get("clickActivity") as String
        val appId = task?.broadcastRequest?.subscriberFetchCriteria?.get("appId") as String

        return notificationApiClient.sendIndividualNotifications("${task.taskId}-${subscriber.id}",
                arrayOf(subscriber.id), messageTitle, messageData, dispatchType, clickAction, hashMapOf("appId" to appId))
                .map { resp ->
                    if (resp.isSuccessful()) {
                        DispatchResult.success(subscriber)
                    } else {
                        DispatchResult.permanentFailure(subscriber, "E1000")//todo gayan resolve the result code properly
                    }
                }
                .block()
    }

    override fun doSend(subscribers: MutableList<Subscriber>?, task: Task?): MutableList<DispatchResult> {
        LOGGER.info("Sending notification to [$subscribers] for task [${task?.taskId}]")
        val messageData = task?.broadcastRequest?.messageData?.get("message") as String
        val messageTitle = task?.broadcastRequest?.messageData?.get("title") as String
        val dispatchType = task?.broadcastRequest?.messageData?.get("dispatchType") as String
        val clickAction = task?.broadcastRequest?.messageData?.get("clickActivity") as String
        val appId = task?.broadcastRequest?.subscriberFetchCriteria?.get("appId") as String

        val subsIdList = Flux.fromIterable(subscribers)
                .map { subscriber -> subscriber.id }
                .collectList()
                .block()
        return notificationApiClient.sendIndividualNotifications("${task?.taskId}-${task?.taskId}",
                subsIdList.toTypedArray(), messageTitle, messageData, dispatchType, clickAction, hashMapOf("appId" to appId))
                .flatMapMany { resp ->
                    Flux.fromIterable(subscribers)
                            .map { subscriber ->
                                if (resp.isSuccessful()) {
                                    DispatchResult.success(subscriber)
                                } else {
                                    DispatchResult.permanentFailure(subscriber, "A5000")//todo gayan resolve the result code properly
                                }
                            }
                }.collectList()
                .block()
    }
}