package hms.appstore.notification.admin.broadcast

import hms.appnotification.repo.repository.BroadcastSubscriberInfoRepo
import hms.common.broadcast.engine.BroadcastEngine
import hms.common.broadcast.engine.SimpleRunningTaskManager
import hms.common.broadcast.engine.StaticSubscriberBatchSizeCalculator
import hms.common.broadcast.repo.TaskRepository
import hms.common.broadcast.repo.mongo.MongoTaskRepository
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.stereotype.Component

@Configuration
class BroadcastEngineConfig(val taskRepository: TaskRepository,
                            val broadcastSubscriberInfoRepo: BroadcastSubscriberInfoRepo,
                            val notificationSender: NotificationSender) {

    @Bean(initMethod = "init", destroyMethod = "stop")
    fun createBroadcastEngine(): BroadcastEngine {
        val engine: BroadcastEngine = BroadcastEngine()

        val batchSizeCalc = StaticSubscriberBatchSizeCalculator()
        batchSizeCalc.setDefaultBatchSize(10)

        val runningTaskManager = SimpleRunningTaskManager()

        engine.setBatchSizeCalculator(batchSizeCalc)
        engine.setTaskRepository(taskRepository)
        engine.setSubscriberRepo(broadcastSubscriberInfoRepo)
        engine.setInitialDelay(4)
        engine.setTaskExecutorPoolSize(10)
        engine.setExecutionDelay(10)
        engine.setMaxRetryCount(5)
        engine.setRunningTaskManager(runningTaskManager)
        engine.setMessageSender(notificationSender)

        return engine
    }
}

@Component
class BroadcastTaskRepository(val mongoTemplate: MongoTemplate) : MongoTaskRepository(mongoTemplate.db)