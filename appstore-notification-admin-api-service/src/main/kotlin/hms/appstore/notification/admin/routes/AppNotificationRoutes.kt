package hms.appstore.notification.admin.routes

import hms.appstore.notification.admin.AppNotificationService
import hms.appstore.notification.admin.BuildFileStatusNotificationListnerService
import hms.appstore.notification.admin.broadcast.BroadcastEngineConfig
import hms.appstore.notification.admin.model.BuildFileStatusRequest
import hms.appstore.notification.admin.model.NotifyUsersRequest
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.body
import org.springframework.web.reactive.function.server.bodyToMono
import org.springframework.web.reactive.function.server.router

@Configuration
@Import(BroadcastEngineConfig::class)
class AppNotificationRoutes(var appNotificationService: AppNotificationService,
                            var buildFileNotificationListner: BuildFileStatusNotificationListnerService) {

    @Bean
    fun wellnessCoreApiRouter() = router {
        (accept(MediaType.APPLICATION_JSON) and "/appstore/admin/notification/v1").nest {
            "/notify".nest {
                POST("/", {
                    ServerResponse.ok().json().body(appNotificationService.notifyUsers(it.bodyToMono<NotifyUsersRequest>()))
                })
            }
            "/buildFile".nest {
                POST("/", {
                    ServerResponse.ok().json().body(buildFileNotificationListner.buildFileStatusUpdate(it.bodyToMono<BuildFileStatusRequest>()))
                })
            }
        }
    }

    fun ServerResponse.BodyBuilder.json() = contentType(MediaType.APPLICATION_JSON_UTF8)
}