package hms.appstore.notification.admin.model

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo

data class NotifyUsersRequest(
        val requestId: String,
        val recipient: MessageRecipient,
        val messageTitle: String,
        val messageContent: String,
        val targetPage: String?,
        val createdBy: String,
        val dispatchType: String?
)

data class NotifyUserResponse(val requestId: String)


@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "recipientType")
@JsonSubTypes(value = [
JsonSubTypes.Type(value = AllUsers::class, name = "All"),
JsonSubTypes.Type(value = AppSpecific::class, name = "AppSpecific")
])
interface MessageRecipient {
    val recipientType: String
}

class AllUsers : MessageRecipient {
    override val recipientType: String = "All"
}

data class AppSpecific(var appId: String) : MessageRecipient {
    override val recipientType: String = "AppSpecific"
}

