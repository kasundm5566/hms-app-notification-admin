package hms.appstore.notification.admin.model

data class BuildFileStatusRequest(
        val requestId: String,
        val appId: String,
        val appName: String,
        val buildVersion: String,
        val status: String,
        val dispatchType: String?
)

data class BuildFileStatusResponse(
        val status: String,
        val taskId: String
)