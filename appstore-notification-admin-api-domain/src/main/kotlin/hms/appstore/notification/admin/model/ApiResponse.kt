package hms.appstore.notification.admin.model

import org.reactivestreams.Publisher

/**
 * Generic API response
 */
interface ApiResponse<T>

/**
 * API response to be used with success cases.
 * Will result in HTTP 200.
 */
data class SuccessApiResponse<T>(val response: Publisher<T>) : ApiResponse<T>

/**
 * API response to be used with not found cases.
 * Will result in HTTP 404.
 */
data class ItemNotFoundApiResponse<T>(val request: T) : ApiResponse<T>

data class ErrorResponse(var errorCode: String,
                         var errorDescription:String) {
}

