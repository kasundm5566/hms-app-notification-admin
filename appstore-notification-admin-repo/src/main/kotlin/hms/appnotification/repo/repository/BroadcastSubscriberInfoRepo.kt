package hms.appnotification.repo.repository

import com.mongodb.BasicDBObject
import hms.common.broadcast.domain.Subscriber
import hms.common.broadcast.repo.mongo.AbstractMongoSubscriberRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.stereotype.Component
import java.util.ArrayList

@Component
class BroadcastSubscriberInfoRepo(val mongoTemplate: MongoTemplate) : AbstractMongoSubscriberRepository(mongoTemplate.db) {

    companion object {
        val LOGGER: Logger = LoggerFactory.getLogger(BroadcastSubscriberInfoRepo::class.java)
    }

    override fun fetchNextBatch(taskId: String,
                                fetchCriteria: MutableMap<String, Any>,
                                lastSentIndex: Int,
                                batchSize: Int,
                                estimatedTotalSubscriberCount: Long): List<Subscriber> {

        LOGGER.info("Finding subscribers for task [$taskId] for [$fetchCriteria], skip[$lastSentIndex] batch [$batchSize]")

        val collection = this.mongoTemplate.db.getCollection("download_info", BasicDBObject::class.java)

        val subscriberList = collection.find(BasicDBObject("appId", fetchCriteria["appId"]))
                .limit(batchSize)
                .skip(lastSentIndex)
                .into(ArrayList<BasicDBObject>())

        return subscriberList.map { entity ->
            val subscriber = Subscriber()
            subscriber.id = entity.getString("msisdn")
            subscriber
        }
    }
}