package hms.appstore.notification.admin.repo.repository

import hms.appstore.notification.admin.repo.entity.DeviceRegistrationEntity
import hms.appstore.notification.admin.repo.entity.DeviceRegistrationEntityId
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Repository
@Transactional
interface DeviceRegistrationRepository : JpaRepository<DeviceRegistrationEntity, DeviceRegistrationEntityId>